import java.util.concurrent.Semaphore;

/**
 * Classe implémentant le comportement d'un process
 */
public class Process implements Lamport, Runnable {

    //Attributs du process
    private Thread thread;
    private boolean alive;
    private boolean dead;

    private Com com;

    //Attributs Lamport
    private int clock;
    private Semaphore semaphore;

    /**
     * Constructeur d'un process
     *
     * @param name Le nom du process
     */
    Process(String name){
        //Initialisation process

        this.thread = new Thread(this);
        this.thread.setName(name);
        this.alive = true;
        this.dead = false;
        this.com = new Com(this);

        //Initialisation Lamport
        this.semaphore = new Semaphore(1);
        this.clock = 0;


        this.thread.start();
        System.out.println(name+ " alive !");
    }

    /**
     * Comportement métier du process
     */
    public void run(){
        int tempsPasse = 0;
        Object activeMessage;

        while(alive){
            try{
                Thread.sleep(1000);
                if (thread.getName().equals("P1")){

                    this.setClock(getClock()+1);
                    //this.broadcast("Message broadcasté par P1");

                    requestSC();
                    this.sendTo("Message envoyé à P" + (tempsPasse % ExempleUtilisationProcess.nbProcess + 1), tempsPasse % ExempleUtilisationProcess.nbProcess);
                    releaseSC();

                } else {
                    activeMessage =  this.com.fetchMessage();
                    if (activeMessage != null){
                        System.out.println("At clock " + this.getClock() + ", " + this.thread.getName() + " received : " + activeMessage.toString());
                    }
                }
                synchronize();
                tempsPasse++;
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        // liberation du bus
        this.killCom();
        System.out.println(Thread.currentThread().getName() + " stopped, lamport = '" + this.getClock() + "'");
        this.dead = true;
    }

    /**
     * Arrête le process
     */
    void stop(){
        this.alive = false;
    }

    /**
     * Détruit le Com associé au process
     */
    private void killCom(){
        this.com.killCom();
        this.com = null;
    }

    /**
     * Demande au com de broadcast un message
     *
     * @param objet Le contenu du message
     */
    private void broadcast(Object objet){
        this.com.broadcast(objet);
        System.out.println(thread.getName() + " broadcast '" + objet.toString() + "' at " + this.getClock());
    }

    /**
     * Demande au com d'envoyer un message à un process en particulier
     *
     * @param objet Le contenu du message
     * @param to L'id du process destinataire
     */
    private void sendTo(Object objet, int to) {
        this.com.sendTo(objet, to);
        System.out.println(thread.getName() + " send '" + objet.toString() + "' to process P" + (to + 1) + " at " + this.getClock());
    }

    /**
     * Demande au com d'enter en zone critique
     * On attend activement qu'il obtienne le token avant de continuer
     */
    private void requestSC(){
        this.com.requestToken();
        System.out.println(thread.getName() + " requested Token at " + this.getClock());
        while(!this.com.hasToken() && this.alive){
            //En attente de l'obtention effective du token
            try {
                Thread.sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //Ici, le process est en section critique car le Com garde le token tant qu'on en a besoin
    }

    /**
     * Indique au com que l'on sort de section critique
     */
    private void releaseSC() {
        this.com.releaseToken();
        //Ici, on a indiqué au Com qu'il n'a plus besoin de garder le token.
        System.out.println(thread.getName() + " released Token at "+ this.getClock());
    }

    /**
     * Demande au com de synchroniser les process
     */
    private void synchronize() {
        System.out.println(this.thread.getName() + " start synchro at " + this.getClock());
        this.com.synchronize();
        do {
            try{
                Thread.sleep(100);
            }catch(Exception e){
                e.printStackTrace();
            }
        } while(this.com.isWaiting());

        System.out.println(this.thread.getName() + " end synchro at " + this.getClock());
    }

    //Implementation Lamport

    /**
     * @return Le temps actuel de l'horloge
     */
    public int getClock() {
        return clock;
    }

    /**
     * Empeche l'accès en écriture à l'horloge
     *
     * @throws InterruptedException exception levée par le sémaphore
     */
    public void lock_clock() throws InterruptedException {
        semaphore.acquire();
    }

    /**
     * Débloque l'accès en écriture à l'horloge
     */
    public void unlock_clock() {
        semaphore.release();
    }

    /**
     * Modifie la valeur de l'horloge
     *
     * @param clock la nouvelle valeur
     * @throws InterruptedException exception levée par le sémaphore
     */
    public void setClock(int clock) throws InterruptedException {
        lock_clock();
        this.clock = clock;
        unlock_clock();
    }

}