/**
 * Type de message associé au broadcast
 */
public class BroadCastMessages extends Messages{

    private int sender;

    /**
     * Constructeur d'un message de type broadcast
     *
     * @param sender L'id du com expéditeur
     * @param objet Contenu du message à envoyer
     * @param horloge Valeur de l'horloge au moment de l'envoie
     */
    BroadCastMessages(int sender, Object objet, int horloge){
        this.sender = sender;
        this.objet = objet;
        this.horlogeLamport = horloge;
    }

    /**
     * Renvoie l'expéditeur du message
     *
     * @return L'id du com expéditeur
     */
    int getSender() {
        return sender;
    }

}