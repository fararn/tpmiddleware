/**
 * Interface permettant de voir le process comme une horloge de Lamport
 */
public interface Lamport {

    /**
     * @return Le temps actuel de l'horloge
     */
    int getClock();

    /**
     * Empeche l'accès en écriture à l'horloge
     *
     * @throws InterruptedException exception levée par le sémaphore
     */
    void lock_clock() throws InterruptedException;

    /**
     * Débloque l'accès en écriture à l'horloge
     */
    void unlock_clock();

    /**
     * Modifie la valeur de l'horloge
     *
     * @param clock la nouvelle valeur
     * @throws InterruptedException exception levée par le sémaphore
     */
    void setClock(int clock) throws InterruptedException;





}
