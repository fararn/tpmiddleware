/**
 * Classe abstraite Message dont hériteront les différents types de messages
 */
public abstract class Messages {

    protected Object objet;

    protected int horlogeLamport;

    /**
     * Renvoie le contenu du message
     *
     * @return l'objet contenu dans le message
     */
    Object getContent() {
        return this.objet;
    }

    /**
     * Renvoie la valeur de l'horloge du message
     *
     * @return la valeur
     */
    int getHorlogeLamport() {
        return horlogeLamport;
    }

}