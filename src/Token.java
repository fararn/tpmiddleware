/**
 * Type de message indiquant la qu'un process est en séction critique
 */
public class Token {

    private int position;

    /**
     * Constructeur du token
     *
     * @param position Indique le com auquel le token est dédié
     */
    Token(int position){
        this.position = position;
    }

    /**
     * Renvoie la position voulue du token
     *
     * @return l'id du process qui doit recevoir le token
     */
    int getPosition() {
        return position;
    }
}
